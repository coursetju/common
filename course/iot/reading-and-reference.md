

2022年全球十大突破性技术，2022.04.10，https://mp.weixin.qq.com/s/JwCOKnAr_y-b8cZTbvv0zA
解读：PoS 权益证明（Proof of stake）是Block chain领域核心技术，而Block chain是推动物联网从传统以通信和感知为主，走向智能物联网，特别是支持智能单体间进行“交易”的关键支撑技术。虽然IoT课程不会讲这些，但同学们应理解物联网和自主交易的关系。

6G技术大会｜零功耗通信白皮书（Zero Power Communication），2022.04.10，https://mp.weixin.qq.com/s/0kp6u9tDnYNd9Vt0RpvwtQ
解读：这主要是由原先的移动通信服务领域所推动，事实上，从5G往后，物联网上升为移动通信领域服务的主要对象，而不再是像以前一样只是个附带的边缘业务。


