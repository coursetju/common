## Practice for IoT Course Learning (Full Version)

## 面向对象 
- 主修专业为自动化专业的同学。
- 信息类专业同学（如AI、EE等）修读该课程时，参照自动化专业要求，不会因专业来源不同而降低要求。
- 3学分版物联网课程

## 目标
深入理解IoT、计算机网络方面的基本原理，掌握主流和典型的开发技术，具备模块级、设备级的开发能力，也具备系统级的组装集成能力。

## 实验平台
### 平台1：基于嵌入式开发板

Cortex-M3 或 Cortex-M4内核，要求自己连接和读取传感器，并进行传输。

目标：重点掌握智能单体设备的开发能力。(在综合设计与实践A基础上改进和扩展功能)

### 平台2：基于日常计算设备与网络设备
日常计算设备与网络设备包括
- PC机或便携本
- 家里的WiFi热点
- 实验室的交换机路由器
- 虚拟机（VirtualBox或VMware）

目标：
- 学习计算机网络及其配置，并在这一过程中理解计算机网络有关的工作原理
- 基于该平台进行基本的网络通信开发实验。

### 平台3：网络仿真


### 平台4：以手机为代表的移动感知平台

把手机作为一个无线传感器来使用。（特别是疫情期间）


## 实验清单

### 板块1：智能单体设备的开发

如下实验内容根据每年度环境支持等因素选择（疫情期间将减少）

- 实验：开发环境的建立、配置与程序调试
  <br> -以LED闪灯程序为例
- 实验：典型通信芯片解读 (cc2520)
- 实验：基于射频芯片的无线通信实验（收发配对）
- 实验：基于射频芯片的无线通信实验（单芯片与PC机的连接）
- 实验：基于射频芯片的无线通信实验（Sniffer）
- 实验：基于RSSI的测距实验
- 实验：基于测距的三边定位实验
- 实验：flooding路由实验*

Reference
- TI: cc2520, https://www.ti.com/product/CC2520#tech-docs
- Silicon Labs: EFR32MG1 Series 1 Mesh Networking Wireless SoCs for Zigbee and Thread, https://www.silabs.com/wireless/zigbee/efr32mg1-series-1-socs
=> [EFR32MG1 Mighty Gecko Multi-Protocol
SoC Family Data Sheet](https://www.silabs.com/documents/public/data-sheets/efr32mg1-datasheet.pdf)
- Lora
- NB-IoT
- Cisio Packet Tracer, https://www.netacad.com/courses/packet-tracer


### 板块2：计算机网络类实验

- 实验：WiFi热点的配置管理
- 实验：基于虚拟机的路由设置*
- 实验：基于Wireshark的抓包实验
- 实验：基于socket的双机通信实验（windows或linux，要求用C语言和TCP协议）
- 实验：基于socket的多人聊天室实验（windows或linux，要求用C语言，UDP协议或TCP协议）

Reference

- Wireshark, https://www.wireshark.org/  [downloads](https://www.wireshark.org/download.html) [wiki](https://wiki.wireshark.org/Home) [Documentation](https://www.wireshark.org/docs/wsug_html_chunked/)
- wireshark使用教程, https://zhuanlan.zhihu.com/p/92993778
- Wireshark中文使用教程（用户版），https://www.wangan.com/docs/wiresharkuser
3分钟搞懂wireshark抓包，https://www.bilibili.com/video/BV1eK4y1Z7ap
- Wireshark零基础入门到实战/网络抓包/流量分析必备，https://www.bilibili.com/video/BV1B5411h7t4/
- wireshark从入门到精通，https://www.bilibili.com/video/BV1YW411y7nr/
- 计算机网络实验第十次---wireshark抓包（rip，icmp，arp），https://www.bilibili.com/video/BV1Ut411d7RE?p=5
- 一篇就够了！全网最全计算机网络基础总结攻略！https://zhuanlan.zhihu.com/p/349337101

### 板块3：网络仿真

- 实验：计算机网络仿真* ([ns3](https://www.nsnam.org/))
- 实验：基于 Cisco Packet Tracer 的计算机网络仿真实验 ([阅读](https://blog.csdn.net/qq_45237293/article/details/111568617))
- 实验：无线传感器网络仿真(JProwler)


Reference
- Cisco Packet Tracer (All Versions) For Windows, Linux and Mac Download, https://windows10freeapps.com/download-cisco-packet-tracer-for-windows-and-linux/
- What is Simulation, https://www.simul8.com/what-is-simulation (不需要看Simu8，只需要通过该文了解什么是simulation)
- What is Network Simulation : Types and Its Advantages, https://www.elprocus.com/what-is-network-simulation-types-and-its-advantages/
- 网络仿真软件对比, https://www.cnblogs.com/godfriend/p/12735712.html
- Cisio Packet Tracer, https://www.netacad.com/courses/packet-tracer
- 18 Network Simulation Software Tools for Certification Practice or Research, https://www.networkstraining.com/network-simulation-software-tools/
- A Survey of Network Simulation Tools: Current Status and Future Developments, https://www.cse.wustl.edu/~jain/cse567-08/ftp/simtools/index.html ([Pdf](https://www.cse.wustl.edu/~jain/cse567-08/ftp/simtools.pdf))
- JSIM: A Java-Based Simulation and Animation Environment, http://cobweb.cs.uga.edu/~jam/jsim/
-> JSim at github, https://github.com/NSR-Physiome/JSim (last modified in 2021) (转向了，不再做network simulation了?)  
- JProwler, http://w3.isis.vanderbilt.edu/projects/nest/jprowler/  [[Doc](http://w3.isis.vanderbilt.edu/projects/nest/jprowler/javadoc/net/tinyos/prowler/package-summary.html)]  (代替Matlab版的[Prowler](http://w3.isis.vanderbilt.edu/projects/nest/prowler/))
- 计算机网络简明教程及仿真实验, https://www.bilibili.com/video/BV1Ut411d7RE?p=5
基于 Cisco Packet Tracer 的计算机网络仿真实验 ([阅读](https://blog.csdn.net/qq_45237293/article/details/111568617))
- Cisio Packet Tracer, https://www.netacad.com/courses/packet-tracer
- PnetLab, https://pnetlab.com/pages/main
- TheVirtualBrain: Brain Network Dynamics Simulator, https://zhuanlan.zhihu.com/p/157236772
- JSL-Java Simulation Library, https://github.com/rossetti/JSL
- JaamSim is a free and open source discrete-event simulation software which includes a drag-and-drop user interface, interactive 3D graphics, input and output processing, and model development tools and editors. https://jaamsim.com/  (open source)
- logisim, https://sourceforge.net/projects/circuit/
- JSIM Superconducting Circuit Simulator, https://github.com/coldlogix/jsim
- JSim:Java-based discrete event simulator of an M/M/s queue system, 2015, https://sourceforge.net/projects/jsim/
- JSim, https://cobweb.cs.uga.edu/~jam/
- JSim, https://github.com/martimy/JSim

- Top 10 Most Popular Network Simulation Tools, 2021,https://www.imedita.com/blog/top-10-list-of-network-simulation-tools/

关于Simulation的原理
- Java Simulation Library (JSL): an open-source object-oriented library for discrete-event simulation in Java [[link](https://www.semanticscholar.org/paper/Java-Simulation-Library-(JSL)%3A-an-open-source-for-Rossetti/2d560eb6a338588722a5ec8c6dacab6d6e40a317)]
- Manuel D. Rossetti, JSL-Java Simulation Library, 2022, https://rossetti.github.io/JSLBook/ (open source book: Simulation modeling in Java)
- The Top 8 Free and Open Source Simulation Software, https://www.goodfirms.co/blog/the-top-8-free-and-open-source-simulation-software
- CS 6910: Systems Modeling and Simulation, https://cs.wmich.edu/alfuqaha/Spring10/cs6910/
- CSci 4210/6210 - Simulation and Modeling, https://cobweb.cs.uga.edu/~jam/home/courses/csci4210/index.html 
- John A. Miller, Simulation and Modeling in Java, https://cobweb.cs.uga.edu/~jam/home/courses/csci4210/book.html
- EE392m: Control Engineering in Industry, https://web.stanford.edu/class/ee392m/ [[2008-2009]()] | [[2004-2005](https://web.stanford.edu/class/archive/ee/ee392m/ee392m.1056/)(lecture 9: Modeling & Simulation)  ([Stanford Industrial AI (I-AI) Initiative](https://iai.stanford.edu/))  ([[SYSTEMX ALLIANCE](https://systemx.stanford.edu/)])
- Modelling & Simulation - Introduction, https://www.tutorialspoint.com/modelling_and_simulation/modelling_and_simulation_introduction.htm
- Building Software for Simulation: Theory and Algorithms, https://onlinelibrary.wiley.com/doi/book/10.1002/9780470877999
- The JSIM web-based simulation environment, https://www.sciencedirect.com/science/article/abs/pii/S0167739X99001089
- J-Sim: A Simulation and emulation environment for wireless sensor networks, IEEE Wireless Communications, 2006
- JSim – Queue Simulator, http://adhocnode.com/jsim-queue-simulator/



### 板块4：基于移动端平台（手机）的实验

- 实验1：手机传感器读取实验
- 实验2：基于TCP/IP协议的手机与计算机的通信
- 实验3：多传感器数据融合实验

Reference
- 与Android移动平台传感器有关的项目 [[Gitee](https://search.gitee.com/?skin=rec&type=repository&q=android%20sensor)] | [[Github]()]
- SensorViewerA, https://gitee.com/extexplorer/SensorViewerA
- SensorViewerPC, https://gitee.com/extexplorer/SensorViewerPC


### Other Reference 

Principles of Software Design, 2020, https://www.geeksforgeeks.org/principles-of-software-design/

### History
2022.03 初稿 

