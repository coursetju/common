## 人工智能基础学习指导：必备要求



### 学习本课程的必备要求

本课程的学习需要进行较多的程序设计与编码实践，要求提前自学，其中Python程序设计与开发为必备要求（第一推荐），C++和Java为可选。

| 语言   | 要求                                                         |
| ------ | ------------------------------------------------------------ |
| Python | - 掌握用Python语言进行数据结构和算法的开发 <br>- 能够用Python结合PyQT或PySide进行简单的GUI应用开发 <br>- 能够用Python结合Flask框架进行简单的服务端程序开发，包括遵循RESTful接口规范的API服务开发和BluePrint插件的简单使用，但禁用服务端的模版技术来开发UI界面 <br>- 能够存取SQLite数据库 <br>- 能够用HTML、vuejs、Element UI、WebPack进行简单的前端程序开发并提供人机交互 <br>- 能够用Python开发所需要的数据结构对象 |

绝大多数任务中，Python都能较好的胜任。但是如果需要自己实现较为复杂的数据结构，以及对算法效率有较高要求，则C++或Java会更合适。但是对是否一定学习C++和Java，不做严格要求。但在这两种语言中，建议优先使用Java，原因是C/C++版的数据结构实现（STL库和Boost库）使用上较Java版数据结构比较复杂，出错概率高，编译速度慢，开发效率较低。如果要开发自己的数据结构，在C/C++体系中要求对底层的STL和Boost有较深入理解才行，相比较之下，在Java里扩展数据接口要更简单一些，最起码垃圾收集器可以简化许多临时对象的申请释放问题。

| 语言 | 要求                                                         |
| ---- | ------------------------------------------------------------ |
| C    | 不推荐在本课程学习中用C语言（尽管在工程项目开发中应用广泛），因为C语言生态中没有标准的数据结构实现。如果要自行开发C语言版的数据结构，需要花费较多的代码和调试时间。 |
| C++  | - 掌握C++中的STL数据结构库及其使用。要想在工程实践中用好C++，STL类库应该掌握。 <br>- 能够使用C++中的Boost类库。需要提醒的是，Boost库的入门需要较长时间，需要理解大量新概念，这是不推荐的主因，且实际中使用频次并不是特别高。如果只是使用还好，但因为我们有自己开发数据结构的需求，在这方面对开发者有较高素质要求。如果有志于将来从事嵌入式系统开发，则投入较多时间学习下STL和Boost也可以考虑。<br>- 能够基于STL或Boost库进一步实现自己的数据结构 |
| Java | - 掌握从Collection开始的数据结构体系 <br>- 能够根据需要开发自己的数据结构 |

实际中，我们也一般很少直接用C++和Java开发用户界面，它们的主要职责是开发API服务。而开发UI界面通常会选择更方便、更快速的解决方案。

| 序              | 已知                               | 对策                                                         |
| --------------- | ---------------------------------- | ------------------------------------------------------------ |
| UI应用开发方案1 | 如果程序核心部分都是用Python开发的 | 则自然使用Python + QT作为UI开发的解决方案。                  |
| UI应用开发方案2 | 如果程序核心部分都是用Java开发的   | 则自然将Java模块包装成一个API服务器，然后通过 vuejs + html 方式开发网页版交互界面即可。 |
| UI应用开发方案3 | 如果底层是C++开发的                | 有两条道路开发UI，一是将底层C++模块扩展成一个就UDP对外提供API调用的底层程序，然后人机交互部分用Python，或者是Python直接调用C++版的DLL。 |

### 

### 学习环境安装与设置

Python解释器和执行环境

https://www.python.org/downloads/

要求3.8及以上版本

目标安装文件夹： \binx\python-3.8

要求目标路径不能有空格。



编码、运行与调试集成环境

PyCharm、Visual Studio Community、Visual Studio Code三选一。

其中PyCharm为第一推荐



The Jupyter Notebook

https://jupyter.org/

The Jupyter Notebook is an open-source web application that allows you to create and share documents that contain live code, equations, visualizations and narrative text. Uses include: data cleaning and transformation, numerical simulation, statistical modeling, data visualization, machine learning, and much more.



PyMiner

https://gitee.com/py2cn/pyminer

pyminer 是一款基于Python的开源、跨平台数据计算解决方案，通过加载各种插件实现不同的需求，用易于操作的形式，在统一的界面中，通过数据计算实现用户所设想的任务。



### 教程

除了这里开列的网上资源，各位同学可以到网上去找其他资源，例如bilibili上的一些公开课。本课程对此不设强制性要求，只是要求应该在第4周前完成自学，以不影响做实践类题目为准。



#### Python类

##### Python语言和基本使用

- Introduction to Python, http://tdc-www.harvard.edu/Python.pdf  (Harvard简明的PPT)
- Python 基础教程，https://www.runoob.com/python/python-tutorial.html
- Python官方教程 / The Python Tutorial，https://docs.python.org/3/tutorial/index.html
- https://www.python.org/about/gettingstarted/
- https://www.guru99.com/python-tutorials.html
- https://www.learnpython.org/
- https://www.tutorialspoint.com/python/index.htm
- Get started learning Python with DataCamp's free Intro to Python tutorial. Learn Data Science by completing interactive coding challenges and watching videos by expert instructors. Start Now!  （https://www.learnpython.org/）
- https://www.pythontutorial.net/
- 廖雪峰的官方网站，https://www.liaoxuefeng.com/wiki/1016959663602400  （不仅仅是语法介绍，还包括一些工程常用小技术）
- Python基础教程，Python入门教程（非常详细），http://c.biancheng.net/python/   （确实够详细）
- Python 入门指南，http://www.pythondoc.com/pythontutorial3/index.html



##### Python：Flask

- https://www.w3cschool.cn/flask/
- https://flask.palletsprojects.com/en/2.0.x/
- 使用 Python 和 Flask 设计 RESTful API，http://www.pythondoc.com/flask-restful/first.html  （建议重点阅读）
- Python Flask框架：零基础web开发入门教程，2018，https://segmentfault.com/a/1190000017330435  （其中的HTML模版使用不要求，使用SQLAlchemy的数据库集成现阶段也可以不用）。
- python中Flask蓝图的使用方法（附代码），https://www.php.cn/python-tutorials-412948.html
- Flask Tutorial in Visual Studio Code，https://code.visualstudio.com/docs/python/tutorial-flask （很多人用vscode但我感觉调试能力不如PyCharm甚至不如Visual Studio，所以不作为第一推荐）



##### 基于Python和QT库的GUI桌面应用开发

- 两套方案：PySide或PyQT 
- https://www.qt.io/qt-for-python
- 一文带你读懂PyQt：用Python做出与C++一样的GUI界面应用程序，2021，https://blog.csdn.net/LaoYuanPython/article/details/116277230
- 如何评价Qt官方推出的Qt for Python(PySide)？https://www.zhihu.com/question/306793447
- https://pypi.org/project/PySide/
- The complete PySide2 & PySide6 tutorial — Create GUI applications with Python The easy way to create desktop applications，https://www.pythonguis.com/pyside-tutorial/
- tonytony2020 / Qt-Python-Binding-Examples， Lots of simple and Pythonic PySide demos，https://github.com/tonytony2020/Qt-Python-Binding-Examples



##### Python中使用SQLite数据库

选择SQLite数据库是因为它足够简单且使用方便。除了可以存数据到硬盘上的文件，也可以以内存数据库方式运行。要求用DB-API方式而不是SQLAlchemy为代表的ORM映射方式来访问数据库。

- SQLite - Python，https://www.runoob.com/sqlite/sqlite-python.html
- Python sqlite3数据库模块使用攻略， https://zhuanlan.zhihu.com/p/196807781
- 你知道Python有内置数据库吗？Python内置库SQlite3使用指南，https://zhuanlan.zhihu.com/p/165371456
- Python基础系列讲解——如何使用自带的SQLite数据库，https://zhuanlan.zhihu.com/p/73353849
- SQLite 数据库 DB-API 2.0 接口模块，https://docs.python.org/zh-cn/2/library/sqlite3.html
- sqlite3 — DB-API 2.0 interface for SQLite databases，https://docs.python.org/3/library/sqlite3.html
- PEP 249 -- Python Database API Specification v2.0，https://www.python.org/dev/peps/pep-0249/



#### Java及工程数据结构教程

- Data Structure in Java – A Complete Guide for Linear & Non-Linear Data Structures
- https://techvidvan.com/tutorials/data-structure-in-java
- 这里同时也给出了Java的教程
- java数据结构和算法实现，https://github.com/xiaoxinglai/java-Data-structure-and-algorithm
- Java 数据结构，https://www.runoob.com/java/java-data-structures.html  （中文，但简单）
- Data Structures in java，https://java2blog.com/data-structures-java/
- Data Structures Tutorial，https://www.javatpoint.com/data-structure-introduction
- Tree Data Structure, https://www.javatpoint.com/tree
- Data Structures 101: a deep dive into trees with Java, https://www.educative.io/blog/data-structures-trees-java
- The Trie Data Structure in Java, https://www.baeldung.com/trie-java
- Tree Data Structure, https://www.interviewcake.com/concept/java/tree
- Java Graph，https://www.javatpoint.com/java-graph
- Java Graph Tutorial – How To Implement Graph Data Structure，2021，https://www.softwaretestinghelp.com/java-graph-tutorial/
- Java Program to Implement the graph data structure，https://www.programiz.com/java-programming/examples/graph-implementation
- Graph Algorithms and Data Structures Explained with Java and C++ Examples，2020，https://www.freecodecamp.org/news/graph-algorithms-and-data-structures-explained-with-java-and-c-examples/
- Graph Data Structure And Algorithms，https://www.geeksforgeeks.org/graph-data-structure-and-algorithms/
- JGraphT: a Java library of graph theory data structures and algorithms now with Python bindings too!, https://jgrapht.org/



#### C++及工程数据结构教程

- The C++ Standard Template Library (STL)，https://www.geeksforgeeks.org/the-c-standard-template-library-stl/
- C++ STL Tutorial，https://www.tutorialspoint.com/cplusplus/cpp_stl_tutorial.htm
- C++ STL (Standard Template Library)，https://www.includehelp.com/stl/
- https://github.com/microsoft/STL
- C++ Standard Library Containers，https://docs.microsoft.com/en-us/cpp/standard-library/stl-containers?view=msvc-160
- C++ Standard Library reference，https://docs.microsoft.com/en-us/cpp/standard-library/cpp-standard-library-reference?view=msvc-160
- STL Algorithms in C++，https://www.delftstack.com/howto/cpp/stl-algorithms-in-cpp/
- https://www.cplusplus.com/reference/stl/
- STL教程：C++ STL快速入门（非常详细），http://c.biancheng.net/stl/
- Boost Library, https://www.boost.org/





修订历史

2021.08.05 by 张伟

初稿

















