## 数据工程学习指南

----

数据工程课学习的产出之一，或者说学习目标之一，是要借数据分析全流程技术的学习，掌握企业级信息系统的设计与开发，毕竟数据的收集、分析、可视化、人机交互以及运维全流程是企业级信息系统开发的核心任务。但技术在实际中一般不单独存在，如果只是掌握了若干算法，还不足以完成一个企业系统的构建并在实际中创造价值，因此，作为技术体现的所谓数据分析算法仍需要融合在应用中以 Total Solution 的形式打包给客户。故我们把数据工程课的学习目标之一定为掌握企业级信息系统的设计与开发，并在这门课中高度重视企业级信息系统开发技术的学习与训练。

依据此思想，数据工程课的学习重点包括如下板块：
- 数据库及其应用（包括数据库原理、数据应用的设计与开发，主要限于单机应用，即传统的数据库原理课内容）
- 分布式数据系统及其架构
- 数据挖掘算法（即传统的数据挖掘课程内容）
- 案例学习

大致上可以认为：
数据工程课 = 数据库原理课 + 数据挖掘课 + 典型案例学习

<br/>

-----
### Recommended Developing Learning

### SQL Tutorials

- SQL Tutorial, W3Schools, https://www.w3schools.com/sql/default.asp (可以直接在网页上输入SQL并观察结果)
- SQL Tutorial for Beginners: Learn SQL in 7 Days，
https://www.guru99.com/sql.html
- Learn SQL, https://www.guru99.com/sql.html
- https://www.geeksforgeeks.org/sql-tutorial/
- https://www.runoob.com/sql/sql-tutorial.html
- What is Normalization in DBMS (SQL)? 1NF, 2NF, 3NF, BCNF Database with Example，2022， https://www.guru99.com/database-normalization.html


### More 

- [初学者开发指南：云端服务和应用](./../../tutorial/cloud_developing_guide_for_beginners.md)

- [企业应用与系统的设计与开发](https://gitee.com/coursetju/common/blob/master/tutorial/enterprise_developing_guide_for_beginners.md)

<br/>

----

### 修订历史

| Date | Author | Description | State |
| :--- | :--- | ---- | :---: |
| 2021.08.05 | 张伟 | 初稿 | draft |
| 2022.03.06 | Zhang Wei | Draft | released |

















