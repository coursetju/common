
刚哥谈架构（十三）大数据软件开源版图，2021，
https://my.oschina.net/taogang/blog/5085570
=> 了解大数据领域的生态（厂家和产品，不过对于初学者特别是本科生而言，掌握好单机和并发两个要点从学习的角度看最重要）

刚哥谈架构（十四）大数据软件开源版图（续），2021，
https://my.oschina.net/taogang/blog/5118502

刚哥谈架构 （九） 开源关系型数据库架构，2021，
https://my.oschina.net/taogang/blog/4953500
=> 强烈推荐数据工程课同学阅读

刚哥谈架构 （五） 谈谈数据库架构，2020，
https://my.oschina.net/taogang/blog/3167206
=> 面向初学者

database architecture, 
https://dsf.berkeley.edu/papers/fntdb07-architecture.pdf


刚哥的博客
https://my.oschina.net/taogang?tab=newest&catalogId=494098


刚哥谈架构 （四）- 推荐给架构师的书单,
https://my.oschina.net/taogang/blog/3150869
专业技术类推荐了3本
Designing Data-Intensive Applications, https://book.douban.com/subject/26197294/
这本书是分布式设计的经典，讲述的非常的细致和系统，来龙去脉，由浅入深。豆瓣评分9.7可谓好评拉满。对于从事数据系统开发的架构师和程序员是必读书籍。
The Master Algorithm 终极算法 https://book.douban.com/subject/26575738/
无论你是不是从事机器学习行业的，这本书都值得阅读。它可以帮助你梳理对机器学习诸多算法的认识，构建一个完整，系统的体系结构。中文版翻译不如英文原版。
A Philosophy of Software Design https://book.douban.com/subject/30218046/
斯坦福 John Ousterhout的作品，不厚，但是浓缩的都是精华。本书从软件设计应该简单，避免复杂为主要观点出发，介绍了软件设计的一些核心的指导原则和思考。


用Python实现一个大数据搜索引擎,2017,
https://my.oschina.net/taogang/blog/1579204
搜索是大数据领域里常见的需求。Splunk和ELK分别是该领域在非开源和开源领域里的领导者。本文利用很少的Python代码实现了一个基本的数据搜索功能，试图让大家理解大数据搜索的基本原理。第一步我们先要实现一个布隆过滤器。布隆过滤器是大数据领域的一个常见算法，它的目的是过滤掉那些不是目标的元素。也就是说如果一个要搜索的词并不存在与我的数据中，那么它可以以很快的速度返回目标不存在。下面一步我们要实现分词。 分词的目的是要把我们的文本数据分割成可搜索的最小单元，也就是词。有个分词和布隆过滤器这两个利器的支撑后，我们就可以来实现搜索的功能了。更进一步，在搜索过程中，我们想用And和Or来实现更复杂的搜索逻辑。
https://conf.splunk.com/files/2017/slides/a-trip-through-the-splunk-data-ingestion-and-retrieval-pipeline.pdf
=> 从学习的角度看，此文极具参考价值

使用开源软件快速搭建数据分析平台，2016
https://my.oschina.net/taogang/blog/630632
这些产品的目标应该都是self service的BI，利用可视化提供数据探索的功能，并且加入机器学习和预测的功能。它们对标的产品应该是Tableau或者SAP Lumira。因为笔者曾经为Lumira开发数据可视化的功能，对这一块很感兴趣，于是就试用了一下这些产品，感觉这些产品似乎还有很大的差距，于是就想自己用开源软件搭一个简单的数据分析平台试试看。
代码在这里 https://github.com/gangtao/dataplay2 
=> 对自己开发一个数据分析产品十分具有参考价值，就是代码老了些

再谈使用开源软件搭建数据分析平台,2019,
https://my.oschina.net/taogang/blog/3039572
=> 本文基本上更新了绝大多数技术选型，更切合今天的社区软件支持

dataplay3 数据分析软件，
https://www.oschina.net/p/dataplay3 | https://gitee.com/mirrors/dataplay3 | https://gangtao.github.io/dataplay3/ | https://github.com/gangtao/dataplay3 (star 142 till 2022.03, not update in the last 3 years)

用500行纯前端代码在浏览器中构建一个Tableau, 2018，(刚哥博客)
https://my.oschina.net/taogang/blog/1811573
在数据可视化开源领域里，大家对百度开发的echarts可谓耳熟能详，echarts经过多年的发展，其功能确实非常强大，可用出色来形容。但是蚂蚁金服开源的基于The Grammar Of Graphics的语法驱动的可视化库G2，让人眼前一亮。那我们就看看如何利用G2和500行左右的纯前端代码来实现一个的类似Tableau的数据分析功能。
演示参见 https://codepen.io/gangtao/full/OZvedx/
代码参见 https://gist.github.com/gangtao/e053cf9722b64ef8544afa371c2daaee 
=> 初学者学习，数据可视化可以参考


Exchangis 轻量级数据交换平台,
https://www.oschina.net/p/exchangis

ADIOS2 可适应输入/输出系统, 2022,
https://www.oschina.net/p/adios2 | https://github.com/ornladios/ADIOS2 | https://gitee.com/mirrors/adios2


=== Devop ===

记某百亿级mongodb集群数据过期性能优化实践
https://my.oschina.net/u/4087916/blog/5122200?_from=gitee_rec

=== Application ===

WRF 气象研究与预报建模系统
https://www.oschina.net/p/wrf
WRF 是“Weather Research and Forecasting (WRF) model”（气象研究与预报模式）的官方资料库。
WRF 是设计用于气象研究和数值天气预报的最先进的大气建模系统，它为大气过程提供了许多选项，并且可以在各种计算平台上运行。WRF 在数十米到数千公里的范围内的各种应用中均表现出色。

MAKRO 丹麦政府经济模型, 2022,
https://www.oschina.net/p/makro | https://gitee.com/mirrors/MAKRO | https://github.com/DREAM-DK/MAKRO (star 1K till 2022.03)
MAKRO 是丹麦财政部使用的经济模型，由丹麦经济分析和模型研究所（简称"DREAM"）的 MAKRO 模型小组开发。
作为一个经济模型，MAKRO 建立之初是为了更好地描述丹麦的经济情况，包括短期和长期方面。此外，MAKRO 还被用于分析经济政策举措如何影响经济，包括逐步过渡到长期路径。

Liquid-dsp 数字信号处理库, 2021,
https://www.oschina.net/p/liquid-dsp | https://gitee.com/mirrors/liquid-dsp | https://github.com/jgaeddert/liquid-dsp
提供一个轻量级的 DSP 库，不依赖无数的外部依赖性或专有的和其他繁琐的框架。所有的信号处理元素都被设计成灵活、可扩展和动态的，包括滤波器、滤波器设计、振荡器、调制器、同步器、复杂的数学运算等等。liquid-dsp 只依赖于libc和libm（standard C 和 math）库来运行；但是如果其他库（如FFTW）可用的话，liquid会利用它们。

GATK 基因组分析工具包,
https://www.oschina.net/p/gatk

GiantMIDI-Piano 古典钢琴 MIDI 数据集,
https://www.oschina.net/p/giantmidi-piano


https://github.com/sanic-org/sanic (github star 15.9K)
代替Flask

