
Jeff Dean
https://baijiahao.baidu.com/s?id=1686666476828285326&wfr=spider&for=pc

PanJiachen
Element UI开发领导，字节跳动前端
https://github.com/PanJiaChen/awesome-bookmarks

前端学习
https://my.oschina.net/u/1416844/blog/602538?_from=gitee_rec

naughty 刚哥的博客，
https://my.oschina.net/taogang
=> 系统架构，大数据，数据可视化，部分机器学习。有一些博文的内容还是比较有深度的。

廖雪峰的博客
https://www.liaoxuefeng.com/
适合初学者，他自己还写了一个给自己用的博客：https://github.com/michaelliao/itranswarp。可以学习其代码。

阮一峰
https://www.ruanyifeng.com/
https://github.com/ruanyf
中文技术文档的写作规范，https://github.com/ruanyf/document-style-guide


JEECG官方博客
https://blog.csdn.net/zhangdaiscott?type=blog
JEECG从早年的建站软件发展到今天，已经是一套功能比较完善的开源低代码企业系统快速开发系统。可以学习之。
JEECG开源社区，我们致力于国内开源事业，打造开源的企业级低代码平台JeecgBoot，免费报表工具JimuReport，开源的微信管家平台 JeewxBoot
http://jeecg.com/
https://github.com/zhangdaiscott/jeecg-boot
https://gitee.com/jeecg


一苇渡江694
https://blog.csdn.net/wangshubo1989?type=blog
WebRTC类


涛哥（张开涛）
https://blog.csdn.net/stpeace?type=blog
=> 服务端技术
跟开涛学习Spring MVC，
https://www.iteye.com/blog/jinnianshilongnian-1752171
数年前的服务端开发必读，不过今天因为都基于集成后的SpringBoot，所以对Spring MVC内部了解不深也可以干点小事情。
https://edu.51cto.com/lecturer/14504649.html

华为云开发者社区
https://blog.csdn.net/devcloud?type=blog

阿里云云栖号
https://blog.csdn.net/yunqiinsight?type=blog


博文视点
https://blog.csdn.net/broadview2006?type=blog


SAP剑客
https://blog.csdn.net/zhongguomao
在大型企业软件领域德国的SAP一直是业界领先。
单纯就SAP软件的文档来说，读一读都会对理解企业的运作和具体系统的设计很有帮助。


一个处女座的程序猿
https://blog.csdn.net/qq_41185868
AI类


AI视觉网奇
https://blog.csdn.net/jacke121
AI类

LaoYuanPython
https://blog.csdn.net/laoyuanpython?type=blog
：CSDN 2020年博客之星TOP3。博客主要关注音视频剪辑、数字图像处理、图形界面开发等Python相关知识！ 另有高数、图像处理、OpenCV、Python以及架构类等电子书

中国DBA社区群
https://www.cndba.cn/index.htm

DBAPlus
https://dbaplus.cn/

数据库DBA工程师招聘
https://www.lagou.com/s/list_35501fd64b2bdce158dfbc7840894bdf3b1ebf5f943a4d78bfd4f1794c4916fa