## 简介 Course TongJi Gitee Introduction 

本Git库集合（coursetju）主要面向人工智能与自动化专业实验和实践教学需要，收集了部分课程的相关文档和项目代码，这些资料用于补充或代替实验指导书，便于初学者自学和快速起步。使用者应注意这里的代码和资料如无特别说明，通常并非最终版，学生应在此基础上进一步开发方可形成最终版的提交作品。
目前，本Git集合由同济大学电子与信息工程学院控制系 BigLittle Group (AIoT) 维护。</br>

- 本介绍：https://gitee.com/coursetju
- 参考文档清单：https://gitee.com/coursetju/common
- Wiki，https://gitee.com/coursetju/common/wikis

Mission：**Improve the quality of life for people throughout the world!**

Refer to course's [wiki](https://gitee.com/coursetju/common/wikis/pages) for more!

**目录**
[TOC]

## 初学者教程 / Guide for Beginners (update 2023.08)

| Technique | Application Field | Guide & Link |
| --- | --- | --- |
| 云端应用与系统开发 | 企业级应用（含物联网应用）和互联网行业，以分布式网络化为典型特征 |  [Enterprise Application & System Developing](tutorial/enterprise_developing_guide_for_beginners.md) | 
| | | [Cloud Application Developing](tutorial/cloud_developing_guide_for_beginners.md) |
| 桌面应用开发 | 多是工具型应用场景，例如监控监测系统、企业内部自用的测试软件、各种效率工具办公工具多媒体工具类） | [Desktop Application Developing](tutorial/desktop_developing_guide_for_beginners.md) |
| 移动应用开发 | 多是面向人机交互类应用，如效率工具、VR/XR/MR | [Mobile Application Developing](tutorial/mobile_developing_guide_for_beginners.md) |
| 嵌入式应用与设备开发 | 智能设备开发，数据收集或各类监控系统，物联网，VR-MR-XR开发，以软硬件交互为典型特征 |  [Embedded/IoT Application Developing](tutorial/embedded_developing_guide_for_beginners.md) |
| 电子电路与硬件设备开发 | 设备开发，学习上可以着重小体积便携式低功耗，或高性能两个方向，以及掌握机电系统的设计、控制与集成。对自动化专业同学而言，电和计算机一样也是将来吃饭必备技能| [Electronics Device Developing](tutorial/electronics_developing_guide_for_beginners.md) |

通常，云端应用与系统面向的是，；桌面应用面向的；嵌入式

## 课程指南

- 嵌入式系统（代计算机原理与技术）（大二下）
- 综合设计与实践A（大二暑假，接续嵌入式系统）
- 微机原理与接口技术（停开，其内容并入嵌入式系统）
- 人工智能基础（大三上）
- 智能芯片与系统（大三上，强化硬件开发）
- 算法（大三上，强化软件开发）
- Machine Learning（大三下）
- 数据工程（Big Data & Data Engineering）（大三下）
- 物联网（3学分版）（大三下）
- 物联网（2学分版，华六AI+微专业）
- 智能感知网（智能建造专业）（大三上）
- 无线传感器网络（停开，升级为物联网）
- 综合设计与实践B（大三下）
- 综合设计与实践C（大三暑假或大四上）
- 毕业实训（大四上，将停开）
- 医学与健康信息处理（大四上）
- 医学智能
- 毕业设计（论文）

## 代码库及参考

### 工程C/C++开发学习

这里不强调C/C++的语法，而是给出工程实例，展示实际中的开发套路和使用方法。

- 用ISO/ANSI C语言写数据结构（可参考https://github.com/openwsn/node/tree/master/opennode2010_keil/common/openwsn/rtl，如 rtl_iobuf, rtl_lightqueue）
- 基于C语言和结构体模拟面向对象中的封装
  * https://github.com/openwsn/node/tree/master/opennode2010_keil/common/openwsn/rtl下面的 hal_cpu, hal_led, hal_key, hal_adc, hal_uart等模块的接口定义
  * 以及 https://github.com/openwsn/node/tree/master/opennode2010_keil/common/openwsn/svc 下的诸多模块
- 基于状态机模式和查询驱动的通信协议实现（evolve方法及switch部分）和接口实现（https://github.com/openwsn/node/tree/master/opennode2010_keil/common/openwsn/svc，例如aloha，csma，datatree，flood, gradient, one2many等）
- 数据传输中的排队和消息队列
  * 例如以TiIoBuf对象作为字节队列使用 https://github.com/openwsn/node/blob/master/opennode2010_keil/common/openwsn/rtl/rtl_slipfilter.h 和 https://github.com/openwsn/node/blob/master/opennode2010_keil/common/openwsn/rtl/rtl_slipfilter.c  
  * packet queue的使用 https://github.com/openwsn/node/blob/master/opennode2010_keil/common/openwsn/svc/svc_nio_acceptor.h 和 https://github.com/openwsn/node/blob/master/opennode2010_keil/common/openwsn/svc/svc_nio_acceptor.c  
- C语言中基于函数指针的回调技巧
  * 主要看 hal_foundation.h 和 hal_foundation.c, 体会其中的TiFunInterruptHandler、m_listener、m_int2handler的使用, https://github.com/openwsn/node/blob/master/opennode2010_keil/common/openwsn/hal/hal_foundation.h 和 https://github.com/openwsn/node/blob/master/opennode2010_keil/common/openwsn/hal/opennode2010/hal_foundation.c，
  * 以及 TiFunAssertReport的使用，这里自行实现了一个类似C语言assert的机制，https://github.com/openwsn/node/blob/master/opennode2010_keil/common/openwsn/rtl/rtl_foundation.h 和 https://github.com/openwsn/node/blob/master/opennode2010_keil/common/openwsn/rtl/rtl_foundation.c ）
- 基于C语言开发Windows动态链接库，https://gitee.com/openm2x/openwsn.workstation/tree/master/ioservice
- 基于C语言实现对DLL的动态调用，https://gitee.com/openm2x/openwsn.workstation/tree/master/ioservice  （一般对DLL采用静态链接即可，这里演示了如何动态装载一个DLL并调用之）
- UDP通信案例，且被封装在DLL中，便于今后为C、C++、Python等多种语言调用，https://gitee.com/openm2x/openwsn.workstation/tree/master/ioservice
- 基于UDP实现C/C++程序和Python程序的数据交换
- 基于ZMQ实现C/C++程序和Python程序的数据交换
- 基于共享内存实现C/C++程序和Python程序的数据交换


### 工程Java开发学习/云端服务开发学习

- 基于SpringBoot的微服务开发
- 基于UDP实现Java程序和Python程序的数据交换
- 基于ZMQ实现Java程序和Python程序的数据交换
- 基于vite快速构建Web应用
- 基于Element Plus的Web应用UI开发


### 工程C#开发学习/桌面应用开发学习

- digitalworld-tiny(可用于综合设计与实践B做智能小车的组和综C中包含服务端管理需求的组，提供服务端数字世界的而管理，数字世界 = 静态的地图 + 动态的移动物。本程序为综B课程中小车的开发和端云通信提供支持，并作为综B的考核工具之一。目前已经支持小车位置上报。本项目基于流行的Electron框架开发。from 2021, contributed by Luan Jianing), https://gitee.com/visiontju/visiontju.digitalworld-tiny
- workstation (for learning purpose only. developed with C#/WPF, 嵌入式系统、综合设计与实践ABC课程（特别是综A）参考，contributed by Zhang Wei, from 2020)， https://gitee.com/openm2x/openwsn.workstation
- step-by-step学习如何开发一个上位机程序（工作站版程序），用于嵌入式系统、综合设计与实践A课程中与嵌入式开发板交换数据（作为学习C#和GUI开发的起步），https://gitee.com/openm2x/openwsn.workstation/tree/master/step-by-step


### 工程Python开发学习

- 基于Flask的微服务
- 基于 Python进行图像处理
- 基于 Python进行视频处理
- 基于 Python和Flask开发远程的视频处理服务
 

### 嵌入式系统（计算机原理与技术初级学习）

- 温度传感器数据采集
- 智能小车的控制初学者启动版
- 上位机开发 step-by-step学习如何开发一个上位机程序（工作站版程序），用于嵌入式系统、综合设计与实践A课程中与嵌入式开发板交换数据（作为学习C#和GUI开发的起步），https://gitee.com/openm2x/openwsn.workstation/tree/master/step-by-step
- 上位机 workstation (for learning purpose only. developed with C#/WPF, 嵌入式系统、综合设计与实践ABC课程（特别是综A）参考，contributed by Zhang Wei, from 2020)， https://gitee.com/openm2x/openwsn.workstation
- 传感器模拟程序（不完备），用于与上位机配合并对其进行测试，[sensorsimu · openm2m/openwsn.workstation - 码云 - 开源中国 (gitee.com)](https://gitee.com/openm2x/openwsn.workstation/tree/master/sensorsimu)


### 嵌入式Linux（计算机原理与技术进阶学习）

- 在Windows上基于Visual Studio （Community Version）构建嵌入式Linux开发环境
- 树莓派官网，https://www.raspberrypi.org/
- 树莓派实验室，https://shumeipai.nxez.com/
-  树莓派（以及各种派）使用指南，https://zhuanlan.zhihu.com/p/77585297
-  「树莓派」是什么？普通人怎么玩？，https://www.zhihu.com/question/20859055
-  2021：树莓派入坑指南，2021，https://sspai.com/post/66938
-  树莓派4确认翻车：官方承认硬件设计失败，https://baijiahao.baidu.com/s?id=1638853428823472748&wfr=spider&for=pc （新版本应该已经修正）
- 进程间通信 （IPC） 方法总结，https://www.cnblogs.com/joker-wz/p/11000489.html
- 进程间通信及使用场景，https://www.jianshu.com/p/4989c35c9475


### 综合设计与实践A

- step-by-step学习如何开发一个上位机程序（工作站版程序），用于嵌入式系统、综合设计与实践A课程中与嵌入式开发板交换数据（作为学习C#和GUI开发的起步），https://gitee.com/openm2x/openwsn.workstation/tree/master/step-by-step
- 

### 综合设计与实践B（着重单车/单机器人）

- digitalworld-tiny(可用于综合设计与实践B做智能小车的组和综C中包含服务端管理需求的组，提供服务端数字世界的而管理，数字世界 = 静态的地图 + 动态的移动物。本程序为综B课程中小车的开发和端云通信提供支持，并作为综B的考核工具之一。目前已经支持小车位置上报。本项目基于流行的Electron框架开发。from 2021, contributed by Luan Jianing), https://gitee.com/visiontju/visiontju.digitalworld-tiny
  * 参考：Electron 主进程和渲染进程，https://www.jianshu.com/p/e6612ca76a93
  * C语言的客户端服务，https://gitee.com/visiontju/visiontju.digitalworld-tiny/tree/master/agent
- 机械臂的控制
- 机械臂的路径规划
- 智能小车的路径规划
- 智能小车的控制初学者启动版
- 智能小车/机器人运动控制单元参考（基于STM32）
- 智能小车/机器人自主智能单元参考（基于树莓派和嵌入式Linux）
- PID控制算法
- 支持帧传输的串行通信
- 简易任务调度器（可参考https://github.com/openwsn/node/tree/master/opennode2010_keil/common/openwsn/osx，按照复杂度从简单到复杂有 osx_nano, osx_fifosche, osx_tlsche。至少应该掌握先来先服务的调度实现。应能支持指明时间或周期。可剥夺式调度可暂不考虑）
- 调试支持
- 数据传输中的排队和消息队列
  * 例如以TiIoBuf对象作为字节队列使用 https://github.com/openwsn/node/blob/master/opennode2010_keil/common/openwsn/rtl/rtl_slipfilter.h 和 https://github.com/openwsn/node/blob/master/opennode2010_keil/common/openwsn/rtl/rtl_slipfilter.c  
  * packet queue的使用 https://github.com/openwsn/node/blob/master/opennode2010_keil/common/openwsn/svc/svc_nio_acceptor.h 和 https://github.com/openwsn/node/blob/master/opennode2010_keil/common/openwsn/svc/svc_nio_acceptor.c  
- 上位机GUI，如果需要：
  * 可以基于Electron的digitalworld-tiny改进
  * 或基于综设A的基于C#和WPF的 workstation （或step-by-step的产出）改进


### 综合设计与实践C（着重智能感知、多车协同、端云互动）

- 本实践训练环节中，感知包括对外部环境的感知和对内部姿态的感知，多体协同至少包括应用级的协作（暂不含无中央控制的全分布式协作），端云互动提供定位、通信和端云同步、服务端的优化与指挥管控。本阶段不再以单体的控制为重点（这是综设B的任务），但是又要基于综设B的产出成果。
- 从USB camera中读视频数据
- 基于Kalman滤波和3D加速度传感器/陀螺仪的小车姿态估计
- 智能小车/机器人中的地图表示
- 基于单目摄像头的地图构建
- 基于双目摄像头的地图构建
- 基于激光雷达的地图构建
- 基于超声雷达的地图构建
- 基于二维码的智能小车/机器人定位
- 基于UWB无线通信的智能小车/机器人定位
- 基于UDP实现C/C++程序和Python程序的数据交换，https://gitee.com/openm2x/openwsn.workstation/tree/master/ioservice 
  * UDP server， https://www.cnblogs.com/ichunqiu/p/9200723.html （但这个server太简单了，通常还需要再做一层接口才能更好的被其他模块使用，尤其是这里的while循环要么借用主程序循环，要么采用归入独立线程或进程，否则不易使用。当我们在嵌入式linux中起一个UDP server负责接收来自C语言的图像数据并且对其进行处理和返回结果时，我们应该采用独立进程方式，让这个Python程序以独立进程方式保持运行并随时可使用）
  * 运行于嵌入式linux中的UDP客户端服务模块，https://gitee.com/visiontju/visiontju.digitalworld-tiny/tree/master/agent  （与综B的digitalworld-tiny配套，但到了综C服务端必然要替换为新版本）
- 基于ZMQ实现C/C++程序和Python程序的数据交换
- 基于共享内存实现C/C++程序和Python程序的数据交换
- 基于UDP实现Java程序和Python程序的数据交换
- 基于ZMQ实现Java程序和Python程序的数据交换
- 大疆RoboMaster机器人/智能小车的控制初学者启动版(综合设计与实践C，RoboMaster人工智能挑战赛（Univerisity AI Challenge)的组成部分，from 2020）
- 硬件抽象层/平台抽象层接口
- 综合设计与实践C：自主智能运动体（RoboTax、送餐机器人、智能无人车），https://gitee.com/coursetju/intelliworld-autonomous.git
- 综合设计与实践C：服务端/Mini SuperBrain，https://gitee.com/coursetju/intelliworld-cloud
- 综合设计与实践C：移动端和桌面端，https://gitee.com/coursetju/intelliworld-workstation


### 计算机网络/无线传感器网络/智能感知网/物联网

- UDP通信
- TCP通信
- 基于UDP的聊天室，https://www.cnblogs.com/snailclimb/p/9086483.html  （第三方）
- MQTT客户端的开发
- 多对多消息通信（基于Publish/Subscribe机制的消息内核）
- 多人聊天室（多对多通信）
- openwsn（物联网课程参考，中国开源软件竞赛金奖的组成部分，developed with ANSI/ISO C. from 2006），https://github.com/openwsn/node 请主要看 https://github.com/openwsn/node/tree/master/opennode2010_keil
- 无线传输信号强度虽距离增加而变化的规律以及基于RSSI的距离估计
- 基于RSSI的三点定位算法和定位服务
- 基于UWB的三点定位算法和定位服务

[More]: cloud-developing-for-beginner.md


### 人工智能基础

- 八皇后问题搜索编程（搜索的学习）
- multi-user gobang game 佳宁助教所提供多人对战五子棋初版（Artificial Intelligence课，developed with Python, from 2020）
- 华为全场景AI框架，https://gitee.com/mindspore
 

### 智能芯片与系统（代替原微机原理与接口技术）

- 基于硬件描述语言的简单接口设计（按键）
- 基于硬件描述语言的简单接口设计（八段数码管）
- 基于硬件描述语言的运算器设计
- 基于硬件描述语言的串行通信模块
- 基于硬件描述语言的显示模块
- 基于FPGA的单通道数据采集
- 基于FPGA的多通道数据采集
- 基于FPGA的FFT加速（变声器实验）
- 基于FPGA的嵌入式系统集成实验（集成CPU、软硬件集成，基于ZYNQ）
- 基于硬件描述语言和FPGA的双数据通路实验
- 基于FPGA的卷积加速设计
- 从零开始写RISC V处理器，https://liangkangnan.gitee.io/2020/04/29/%E4%BB%8E%E9%9B%B6%E5%BC%80%E5%A7%8B%E5%86%99RISC-V%E5%A4%84%E7%90%86%E5%99%A8/  和  https://gitee.com/liangkangnan/tinyriscv
- 阿里达摩院开源的平头哥RISC V处理器，https://github.com/T-head-Semi
- 海思
- 寒武纪


### 数据处理/信号处理/生理体征数据分析/机器学习

- sciexplorer，a signal processing/stream data analyzer tool, in particularly for ECG/EEG, https://gitee.com/biglittle/sciexplorer （Developed with Matlab/Java/C，from 2007）



### 附：操作系统相关学习

操作系统相关学习要求重点掌握：应用/任务/进程/线程的概念，调度机制，内存管理机制，进程/线程间的协同机制，进程间通信机制，文件的概念及文件系统，硬件驱动体系以及硬件驱动程序的开发，网络体系，以及多CPU系统带来的对调度、进程间/线程间通信与协同的影响。在本科阶段，总体上限于单机体系。

- 硬核操作系统讲解，https://www.jianshu.com/p/9bb248836715
- 5万字、97 张图总结操作系统核心知识点(上)，https://www.jianshu.com/p/2068a98bb810
- 操作系统复习笔记，https://www.jianshu.com/p/2d57b81135c9
- 最易懂的回调机制说明书，https://www.jianshu.com/p/9e440e6cb7c4
- C++工程师常见的面试题总结，2021，https://www.jianshu.com/p/d6b6afebbca4
- Java中高级核心知识全面解析——从认识Linux操作系统开始（什么是操作系统？分类、内核、用户态与内核态），https://www.jianshu.com/p/577ef6f414bb
- 进程间通信（IPC）：共享内存和消息队列原理详解，http://c.biancheng.net/view/1208.html
- Linux进程间通信(四) - 共享内存，https://zhuanlan.zhihu.com/p/390606384
- Linux进程间通信(三) - 信号，https://zhuanlan.zhihu.com/p/389994233  （Linux/Unix开发得知道信号，初学者以了解为准，至少知道信号有什么用，可能用在哪些地方）
- Linux进程间通信(五) - 信号灯(史上最全)及其经典应用案例，https://zhuanlan.zhihu.com/p/391095517
- 多图详解！10 大 “高性能”开发核心技术，看不懂你找我？https://zhuanlan.zhihu.com/p/381911441
- 从内核看eventfd的实现（基于5.9.9），https://zhuanlan.zhihu.com/p/383395277


### Howto

- Howto:  [如何快速建立 git 管理的本地开发代码库](howto/howto_gittee_repo_startup.md)

## 联系

- biglittle Lab @ TongJi University 

## 持续改进
- last updated on 2023.09.28

</br>  

------------------------------------------------------------

<center>CopyRight @ BigLittle(AIoT) Group，2018-2023</center> 
<center>Department of Control Science & Engineering, College of Electronics & Information</center>
<center>TongJi University</center>

