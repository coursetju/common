## 生活在TJ

[TOC]

### 核酸检测

核酸检测预约链接  https://qywx.wjx.cn/vj/wFzKHtw.aspx，
完成预约后查询链接 https://qywx.wjx.cn/resultquery.aspx?activity=126179919

四平校医院，上午9：00-11：00，周日9：00-11：00，周六不安排

嘉定校区卫生所，同上



### 访客预约

微信中搜索平安同济e行公众号，进入访客预约并填写和提交



### 学校周边的茶馆/咖啡馆

上海同济君禧大酒店，上海，杨浦区，彰武路50号 [地图](https://hotels.ctrip.com/hotels/detail/?hotelId=455020&checkIn=2022-01-27&checkOut=2022-01-28&cityId=2&minprice=&mincurr=&adult=1&children=0&ages=&crn=1&curr=&fgt=&stand=&stdcode=&hpaopts=&mproom=&ouid=&shoppingid=&roomkey=&highprice=-1&lowprice=0&showtotalamt=&hotelUniqueKey=)

星巴克，四平路彰武路交界，彰武路上入口



### 接待和游玩









